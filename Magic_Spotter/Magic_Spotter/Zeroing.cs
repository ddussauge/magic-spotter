﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic_Spotter {
    class Zeroing {
        private int lower { get; set; }
        private int upper { get; set; }
        
        public Zeroing(int distance) {

            int round = (int)((distance + 50) / 100) * 100;

            /* 
             * distance = 1222, lower = 1200, upper = 1300 
             * distance = 1200, lower = 1200, upper = 1300
             */
            if (round < distance) {
                this.lower = round;
                this.upper = round + 100;
            } else if (round > distance) {
                this.lower = round - 100;
                this.upper = round;
            } else {    // ==
                this.upper = round;
                this.lower = round;
            }

        }

        /// <summary>
        /// Calculates the horizontal adjustement depending on distance and speed. 
        /// </summary>
        /// <param name="distance">Integer representing the distance to the target in meters (between 0 and 2500)</param>
        /// <param name="speed">Instance of the class Speed that represents the speed of the target with two attributes : name and value (ex : </param>
        /// <returns></returns>
        public double calculateX(int distance, Speed speed) {
            // TODO : calcul en fonction de la distance, du temps de vol et de la vitesse de la cible
            return 0;
        }

        public double calculateY(bool lower, int distance) {
            // TODO : calcul en fonction de la distance
            /*
                Différence entre le zéro et la distance
                Récupérer les données dans l'objet de stockage des ajustements
                accroissement -> cf McKayTheBoss 
            */
            if (lower) {

            } else {

            }
            return 0;
        }
    }
}
