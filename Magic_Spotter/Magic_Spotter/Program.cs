﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magic_Spotter {
    static class Program {

        static Dictionary<int, Dictionary<int, double>> adjustmentsTable = new Dictionary<int, Dictionary<int, double>>() {
            {
                300, new Dictionary<int, double>() {
                    { -100, 0.3 },
                    { -50, 0.15 },
                    { 0, -0.35 },
                    { 50, -0.5 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                500, new Dictionary<int, double>() {
                    { -100, 1 },
                    { -50, 0.5 },
                    { 0, 0.05 },
                    { 50, -0.7 },
                    { 100, -1 }
                }
            },
            {
                600, new Dictionary<int, double>() {
                    { -100, 1 },
                    { -50, 0.5 },
                    { 0, -0.2 },
                    { 50, -0.8 },
                    { 100, -1 }
                }
            },
            {
                700, new Dictionary<int, double>() {
                    { -100, 1 },
                    { -50, 0.4 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 1 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            },
            {
                400, new Dictionary<int, double>() {
                    { -100, 0.5 },
                    { -50, 0.25 },
                    { 0, -0.25 },
                    { 50, -0.6 },
                    { 100, -1 }
                }
            }
        };



        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Debug.WriteLine(adjustmentsTable[300][-50]);

            Application.Run(new Form1());
        }
    }
}